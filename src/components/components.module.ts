import { NgModule } from '@angular/core';
import { SearchbarComponent } from './searchbar/searchbar';
@NgModule({
	declarations: [SearchbarComponent],
	imports: [],
	exports: [SearchbarComponent]
})
export class ComponentsModule {}
