import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { ComitesPage } from '../pages/comites/comites';
import { VoluntarioPage } from '../pages/voluntario/voluntario';
import { NotificacoesPage } from '../pages/notificacoes/notificacoes';
import { ParticipacaoPage } from '../pages/participacao/participacao';
import { PesquisaPage } from '../pages/pesquisa/pesquisa';
import { ProgramaGovernoPage } from '../pages/programa-governo/programa-governo';
import { HomePage } from '../pages/home/home';
import { MaterialInstitucionalPage } from '../pages/material-institucional/material-institucional';
import { ProgramaPontosPage } from '../pages/programa-pontos/programa-pontos';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { CONFIG_PROJECT } from '../providers/app-config';

import { LoginPage } from '../pages/login/login';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Push, PushObject, PushOptions } from '@ionic-native/push';

import { AuthProvider } from '../providers/auth/auth';
import { PersonProvider } from '../providers/person/person';


import { NotificationProvider } from '../providers/notificacao/notificacao';
import { QueroContribuirPage } from "../pages/quero-contribuir/quero-contribuir";
import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public disconnect: boolean = false;

  rootPage: any;
  profile: any;
  count: 0;

  cordova: any;
  pages = [
    { title: 'Início', component: HomePage, icon: 'home', description: 'Notícias, Eventos, Agenda...',  cssClass: 'iniciocss', parameter : 0 },
    { title: 'Notificações', component: NotificacoesPage, icon: 'notifications', description: null, notificationsNumber: true, parameter : 0 },
    { title: 'Programa de Governo', component: ProgramaGovernoPage, icon: 'list', description: null , parameter : 0},
    { title: 'Seja Voluntário', component: VoluntarioPage, icon: 'hand', description: null , parameter : 0},
    { title: 'Minha Participação', component: ParticipacaoPage, icon: 'person', description: null, parameter : 0 },
    { title: 'Pesquisa de Opinião', component: PesquisaPage, icon: 'text', description: null, parameter : 0 },
    { title: 'Programa de Pontos', component: ProgramaPontosPage, icon: 'star', description: 'Como funciona, histórico de pontos...', cssClass: 'programacss', parameter : 0 },
    { title: 'Material de Campanha', component: MaterialInstitucionalPage, icon: 'images', description: 'Posts, santinhos, banners…', cssClass: 'materialcss', parameter : 0},
    { title: 'Comitês', component: ComitesPage, icon: 'people', description: null, parameter : 0 }
  ];

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public auth: AuthProvider,
    public person: PersonProvider,
    public notification: NotificationProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public events: Events,
    public push: Push,
    public http: Http,
    private network: Network,
) {

    events.subscribe('profile:count', (profile, count) => {
      this.profile = profile;
      this.count = count;
    });

    events.subscribe('profile', (profile) => {
      this.profile = profile;
    });

    events.subscribe('count', (count) => {
      this.count = count;
    });

  
    events.subscribe('menu:closed', () => {
      // your action here
  });
    this.initializeApp();
    this.network.onDisconnect().subscribe(() => {
      this.disconnect = true;
      this.onDisconnect();
    });
  }

  initPushNotification() {
console.log('iniciando app');

    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: '318490138469'

      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on('registration').subscribe((data: any) => {
      console.log('device token -> ' + data.registrationId);
      localStorage.setItem('Token', JSON.stringify(data.registrationId))
      let plataforma = '';
      if (navigator.userAgent.match(/Android/i)) {
          plataforma = 'android';
      } else {
          plataforma = 'ios';
      }

      let pushObj = {
         AppId : CONFIG_PROJECT.appId,
        Token :data.registrationId,
        Plataforma:plataforma
      };

        this.http.post(`${CONFIG_PROJECT.baseApi}`+ 'Api/Push/Register/',pushObj).map(res => res.json())
        //TODO - send device token to server
        .subscribe(
          data => {
           console.log(data, `retorno do token`)
          },
          err => {
            // stop disconnect watch
            console.log('deuruim', err)
          }
          );

    }); 


    pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
  }


  onDisconnect() {
    console.log('Sem conexão com internet. O conteúdo mais recente não poderá ser exibido. :-(');
    this.alertCtrl.create({
    
      title: '<img src="assets/icon/error.png"/>Erro',
      subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
      buttons: ['OK'],
      cssClass: 'alertcss'
    }).present();
  }

  initializeApp() {

console.log('iniciando app');
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.auth.isLogged()) {
console.log('iniciando app logado');

        // this.initPushNotification()
        //this.getNotificationsCount();
        this.rootPage = HomePage;
      }
      else {
        this.rootPage = TutorialPage;
      }
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component,page.parameter);
  }
  
  openPageHistorico(){
    this.nav.setRoot(ProgramaPontosPage,'historico');
  }
  
  openinnap(){
    window.open('https://maisquevoto.com.br/psl', '_blank', 'location=no');
  }
  
  openPagePush(page, params) {
    this.nav.setRoot(page,params);
  }


  logoutApp() {
    this.nav.setRoot(LoginPage);
  }

  
  getNotificationsCount()
  {
    setTimeout(() => { this.notification.Count().then((response: any) => { 
      this.count = response; 
      this.events.publish('count', this.count);
      console.log("sync notification: " + response)
      this.getNotificationsCount();
    })}, 30000);
  }
}

