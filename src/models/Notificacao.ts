import { Injectable } from '@angular/core';
import { IModel } from './IModel';
import { DateTime } from 'ionic-angular/components/datetime/datetime';

@Injectable()
export class Notificacao extends IModel {
    Id: number;
    Titulo: string;
    Mensagem: string;
    Lido: boolean;
    DataCriacao: DateTime;
}