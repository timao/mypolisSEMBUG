export interface Comite {
  Id: number;
  Nome: string;
  Latitude: string;
  Longitude: string;
  Endereco: string;
  Cidade: string;
  Estado: string;
  HorarioFuncionamento: string;
  Telefone: string;
  Email: string;
  Descricao: string;
  DataCriacao: string;
  PartidoNome: string;
}
