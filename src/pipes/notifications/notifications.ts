import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notifications',
})
export class NotificationsPipe implements PipeTransform {

  transform(value: string, ...args): string {
    let valueNum: number = parseInt(value);
    if(valueNum > 999) {
      return "999+";
    }
    return valueNum.toString();
  }
}
