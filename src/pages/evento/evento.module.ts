import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventoPage } from './evento';
import { EventosAmigosPage } from './modal-amigos';

@NgModule({
  declarations: [
    EventoPage,
    EventosAmigosPage
  ],
  imports: [
    IonicPageModule.forChild(EventoPage)
  ],
  entryComponents: [
    EventoPage,
    EventosAmigosPage
  ]
})
export class EventoPageModule {}
