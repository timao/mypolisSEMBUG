import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { PesquisaProvider } from '../../providers/pesquisa/pesquisa';

import { UtilsProvider } from '../../providers/utils/utils';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'pesquisa-opniao',
  segment: 'pesquisa'
})
@Component({
  selector: 'page-pesquisa',
  templateUrl: 'pesquisa.html',
})

export class PesquisaPage {

  listaPesquisa: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public pesquisa: PesquisaProvider,
    public utils: UtilsProvider) {
  }

  ionViewDidLoad() {
    this.getData();
    console.log('ionViewDidLoad PesquisaPage');
  }

  ionViewDidEnter(){
    this.getData();

  }

  presentProfileModal(pesquisa) {
    this.getData();

    if (pesquisa.Finalizada) {
      if (pesquisa.ExibirGrafico) {
        this.getDetail(pesquisa.Id);
      }
      else {
        if (pesquisa.JaVotou) {
          this.utils.showModalSucesso("Você já respondeu essa pesquisa.", null, "OK", null);
        }
        else {
          this.utils.showModalSucesso("Essa pesquisa foi finalizada.", null, "OK", null);
        }
      }
    }
    else {

      if (pesquisa.JaVotou) {
        this.utils.showModalSucesso("Você já respondeu essa pesquisa.", null, "OK", null);
      }
      else {
        let profileModal = this.modalCtrl.create(PesquisaModalPage, pesquisa, {
          showBackdrop: true,
          cssClass: 'modal-box'
        });
        profileModal.present();
      }
    }

  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {

      infiniteScroll.complete();
    }, 500);
  }



  getData() {
    console.log('merda1')

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    setTimeout(() => {
      this.pesquisa.findAll().then((result: any) => {
        loading.dismiss();
        this.listaPesquisa = result;
        console.log(result);

      }, (error) => {
        loading.dismiss();
        console.log(error);
      });
      console.log('passando mto aqui')
    }, 1000);

  }

  getDetail(id: any) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.pesquisa.findById(id).then((result2: any) => {
      loading.dismiss();
      console.log(result2);
      this.navCtrl.push('pesquisa-resposta', result2);
      console.log(result2);
    }, (error2) => {
      loading.dismiss();
      console.log(error2);
    });

  }

  back() {
    this.navCtrl.push(HomePage);
  }


}


@Component({
  selector: 'modal-pesquisa',
  templateUrl: 'pesquisa-modal.html'
})
export class PesquisaModalPage {
  pesquisaDetail: any;
  listaPesquisa: any;
  constructor(
    params: NavParams,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public pesquisa: PesquisaProvider,
    public utils: UtilsProvider) {

    this.pesquisaDetail = params.data;

    console.log('objeto: ', params.data);
  }

  close() {
    this.navCtrl.pop();
  }

  answerQuestion(status) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.pesquisa.post(status).then((result: any) => {
      loading.dismiss();
      this.utils.showModalSucesso(result.Titulo, result.Mensagem, "OK", this.goToPesquisa());
    }, (error) => {
      loading.dismiss();
      this.utils.showModalSucesso("Você já respondeu essa pesquisa.", null, "OK", this.goToPesquisa());

      console.log(error);
    });
  }
  getData() {
    console.log('merda1')

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    setTimeout(() => {
      this.pesquisa.findAll().then((result: any) => {
        loading.dismiss();
        this.listaPesquisa = result;
        console.log(result);

      }, (error) => {
        loading.dismiss();
        console.log(error);
      });
      console.log('passando mto aqui')
    }, 1000);

  }

  goToPesquisa() {
    this.pesquisa.findAll().then((result: any) => {
      this.listaPesquisa = result;
      console.log(result);

    }, (error) => {
      console.log(error);
    });
    this.navCtrl.pop();
  }
}
