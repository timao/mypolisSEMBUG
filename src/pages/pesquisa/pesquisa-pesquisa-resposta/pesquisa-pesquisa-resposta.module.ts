import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesquisaPesquisaRespostaPage } from './pesquisa-pesquisa-resposta';

@NgModule({
  declarations: [
    PesquisaPesquisaRespostaPage,
  ],
  imports: [
    IonicPageModule.forChild(PesquisaPesquisaRespostaPage),
  ],
})
export class PesquisaPesquisaRespostaPageModule {}
