import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PesquisaProvider } from '../../../providers/pesquisa/pesquisa';

/**
 * Generated class for the PesquisaPesquisaRespostaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'pesquisa-resposta',
  segment: 'pesquisa-resposta/:id'
})
@Component({
  selector: 'page-pesquisa-pesquisa-resposta',
  templateUrl: 'pesquisa-pesquisa-resposta.html',
})
export class PesquisaPesquisaRespostaPage {

  pesquisaDetail:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public pesquisa: PesquisaProvider) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesquisaPesquisaRespostaPage');
  }


  getData()
  {
    this.pesquisaDetail =this.navParams.data;
  }

  openPagePush(page) {
    this.navCtrl.push(page);
  }
}
