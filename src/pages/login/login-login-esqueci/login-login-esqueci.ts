import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { AuthProvider } from '../../../providers/auth/auth';
import { UtilsProvider } from '../../../providers/utils/utils';
import { LoginPage } from '../../login/login';




@IonicPage({
  name: 'esqueci-senha'
})
@Component({
  selector: 'page-login-login-esqueci',
  templateUrl: 'login-login-esqueci.html',
})
export class LoginLoginEsqueciPage {

  form: any = {
    Email: ''
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private auth: AuthProvider,
    public utils: UtilsProvider,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginLoginEsqueciPage');
  }

  EsqueciSubmit() {

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    loading.present();

    this.auth.postResetPassWord(this.form.Email)

      .then((response) => {

        setTimeout(() => {
          loading.dismiss();
          this.utils.showModalSucesso(response.toString(), null, "Ok", null);
          // alert.setTitle(response.toString());
          // alert.present();
        }, 1000);
        setTimeout(() => {
          this.navCtrl.setRoot(LoginPage);
        }, 2000);


      }, (error) => {
        console.log(error)
        setTimeout(() => {
          if(error.status == 400){
            console.log('aaaaa')
   
            this.alertCtrl.create({
              title: '<img src="assets/icon/error.png"/> Erro',
              subTitle: error.error.Message,
              buttons: [{ text: 'Ok' }],
              cssClass: 'alertcss'
            }).present();
          }
        }, 1000);
        loading.dismiss();



      });;
  }

  goLogin() {
    this.navCtrl.setRoot('login');
  }

}
