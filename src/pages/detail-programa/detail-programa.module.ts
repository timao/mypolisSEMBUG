import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailProgramaPage } from './detail-programa';

@NgModule({
  declarations: [
    DetailProgramaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailProgramaPage),
  ],
})
export class DetailProgramaPageModule {}
