import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
import { DomSanitizer } from '@angular/platform-browser';
import { PersonProvider } from '../../providers/person/person';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { UtilsProvider } from '../../providers/utils/utils';

@IonicPage()
@Component({
  selector: 'page-detail-transmissao',
  templateUrl: 'detail-transmissao.html',
})
export class DetailTransmissaoPage {
  interna:any
  VideoIframe:any
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController,private _sanitizer: DomSanitizer,
    public person: PersonProvider,) {
    this.interna=this.navParams.get('interna');
    console.log(this.interna)
    this.VideoIframe = this._sanitizer.bypassSecurityTrustResourceUrl(this.interna.Video);
  }
  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailTransmissaoPage');
  }

}
