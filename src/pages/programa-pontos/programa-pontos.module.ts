import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgramaPontosPage } from './programa-pontos';

@NgModule({
  declarations: [
    ProgramaPontosPage,
  ],
  imports: [
    IonicPageModule.forChild(ProgramaPontosPage),
  ],
})
export class ProgramaPontosPageModule {}
