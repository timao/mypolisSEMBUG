import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificacoesNotificacoesDetalhePage } from './notificacoes-notificacoes-detalhe';

@NgModule({
  declarations: [
    NotificacoesNotificacoesDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(NotificacoesNotificacoesDetalhePage),
  ],
})
export class NotificacoesNotificacoesDetalhePageModule {}
