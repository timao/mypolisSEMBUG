import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage({
  name: 'convite',
  segment: 'convite'
})
@Component({
  selector: 'page-convite',
  templateUrl: 'convite.html',
})
export class ConvitePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConvitePage');
  }

}
