import { Component, OnInit, NgModule } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SearchbarComponent } from "../../components/searchbar/searchbar";
import { BrMaskerModule } from 'brmasker-ionic-3';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { DoacaoProvider } from '../../providers/doacao/doacao';
import { CONFIG_PROJECT } from '../../providers/app-config';
import { Stripe } from '@ionic-native/stripe';
import { UtilsProvider } from '../../providers/utils/utils';
import { CurrencyPipe } from '@angular/common';

@IonicPage({
  name: 'quero-contribuir',
  segment: 'quero-contribuir'
})
@Component({
  selector: 'page-quero-contribuir',
  templateUrl: 'quero-contribuir.html',
  providers: [Stripe, CurrencyPipe]
})
@NgModule({
  imports: [
    BrMaskerModule
  ]
})
export class QueroContribuirPage implements OnInit {

  step: number = 1;

  contribuirForm: FormGroup;
  contribuirStep2Form: FormGroup;
  contribuirStep3Form: FormGroup;
  contribuirStep4Form: FormGroup;

  logo: string;
  formaPgto: string = 'cartao';

  bandeira = '';

  configuracao = {
    DoacaoValor1: '',
    DoacaoValor2: '',
    DoacaoValor3: '',
    DoacaoValor4: '',
    DoacaoValorMaximo: 0,
    DoacaoValorMinimo: 0,
    Nome: '',
    Logo: '',
    TextoValorMinimo: '',
    TextoValorMaximo: ''
  };

  model = {
    ClienteId: 0,
    CPFCNPJ: '',
    Nome: '',
    Email: '',
    Celular: '',
    Sexo: 0,
    Valor2: '',
    CEP: '',
    Complemento: '',
    Endereco: '',
    Cidade: '',
    Estado: '',
    Bairro: '',
    EnderecoNumero: '',
    EventoId: null,
    BemServico: '',
    EspecieRecurso: 4,
    StatusSistema: 1,
    NumeroDocumento: '',
    NumeroAutorizacao: '',
    ValidadeDocumento: '',
    NomeDocumento: '',
    DataNascimentoDocumento: '',
    BandeiraDocumento: 0,
    MesDocumento: '',
    AnoDocumento: '',
    check1: false,
    check2: false
  };

  years = new Array();

  erroParte1 = 0.7;
  erroParte2 = 0.7;
  erroParte3 = 0.7;
  erroParte4 = 0.7;
  errorNome = false;
  errorEmail = false;
  errorEmail2 = false;
  errorCelular = false;
  errorCPF = false;
  errorCPF2 = false;
  errorSexo = false;
  errorValor = false;
  errorValor2 = false;
  errorValor3 = false;
  errorCheck1 = false;
  errorCheck2 = false;
  errorCEP = false;
  errorEndereco = false;
  errorCidade = false;
  errorEstado = false;
  errorBairro = false;
  errorNumeroDocumento = false;
  errorNumeroAutorizacao = false;
  errorNomeDocumento = false;
  errorDataNascimentoDocumento = false;
  errorDataNascimentoDocumento2 = false;
  errorDataNascimentoDocumento3 = false;
  errorMesDocumento = false;
  errorAnoDocumento = false;
  errorDataVencimento = false;
  valor1 = false
  valor2 = false
  valor3 = false
  valor4 = false

  constructor(private navCtrl: NavController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    public BrMaskerModule: BrMaskerModule,
    private doacao: DoacaoProvider,
    private loadingCtrl: LoadingController,
    private stripe: Stripe,
    private utils: UtilsProvider,
    private currencyPipe: CurrencyPipe) {
    this.loadAll();


  }

  ngOnInit() {

    //formularios
    this.contribuirForm = new FormGroup({
      nome: new FormControl(this.model.Nome, {
        validators: [Validators.required]
      }),
      email: this.formBuilder.control(this.model.Email, [Validators.required, Validators.email]),
      cpf: this.formBuilder.control(this.model.CPFCNPJ, [Validators.required]),
      celular: this.formBuilder.control(this.model.Celular, [Validators.required]),
      sexo: this.formBuilder.control(this.model.Sexo, [Validators.required])
    });

    this.contribuirStep2Form = new FormGroup({
      valor: this.formBuilder.control(this.model.Valor2, [Validators.required], ),
      check1: this.formBuilder.control(false, [Validators.required]),
      check2: this.formBuilder.control(false, [Validators.required]),
    });

    this.contribuirStep3Form = new FormGroup({
      cep: this.formBuilder.control(this.model.CEP, [Validators.required]),
      endereco: this.formBuilder.control(this.model.Endereco, [Validators.required]),
      cidade: this.formBuilder.control(this.model.Cidade, [Validators.required]),
      estado: this.formBuilder.control(this.model.Estado, [Validators.required]),
      bairro: this.formBuilder.control(this.model.Bairro, [Validators.required]),
      complemento: this.formBuilder.control(this.model.Complemento),
      numero: this.formBuilder.control(this.model.EnderecoNumero),
    });

    this.contribuirStep4Form = new FormGroup({
      recurso: this.formBuilder.control('', [Validators.required]),
      documento: this.formBuilder.control('', [Validators.required]),
      cvv: this.formBuilder.control('', [Validators.required]),
      mes: this.formBuilder.control('', [Validators.required]),
      ano: this.formBuilder.control('', [Validators.required]),
      nomeimpresso: this.formBuilder.control('', [Validators.required]),
      data: this.formBuilder.control('', [Validators.required]),
    });
  }

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
  }

  ionViewDidLoad() {

    var step = parseInt(this.navParams.data.step);

    if (step > 1) {


      this.step = step;

      if (step == 5) {

        var model = this.navParams.data.model;
        if (model.Sucesso == 1) {
          this.utils.showModalSucesso(model.Titulo, model.Mensagem, "OK", null);
        }
      }

    } else {

      this.getDoacaoFromStorage();

      this.step = 1;

    }
  }

  goBack() {
    this.navCtrl.pop().catch(() => {
      this.navCtrl.setRoot('HomePage');
    });
  }

  goToStep(step: number, model, back) {

    let error = false;

    if (step == 2) {
      error = this.validaCampos1(model);
    }
    else if (step == 3) {
      error = this.validaCampos2(model, back);
    }
    else if (step == 4) {
      error = this.validaCampos3(model, false);
    }

    if (!error) {

      //seta a parte em que a pessoa parou a doação
      if (this.model.StatusSistema < step) {
        this.model.StatusSistema = step;
        this.putDoacao();
      }

      //atualizando so dados atuais da doação
      if (model != undefined) {
        localStorage.setItem('doacao', JSON.stringify(model));
      }

      this.navCtrl.setRoot(QueroContribuirPage, { step: step, model: step == 5 ? model : null });
    }
  }

  loadAll() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.carregarConfig().then(() => {
      this.getDoacaoFromStorage();

      this.loadYears();
      loading.dismiss();
    });
  }

  //carrega configuração de valores do cliente
  carregarConfig() {
    return new Promise(resolve => {
      this.doacao.get().then((result: any) => {
        this.configuracao = result;
        this.configuracao.TextoValorMinimo = "Valor mínimo é de R$ " + this.configuracao.DoacaoValorMinimo.toString().replace(".", ",");
        this.configuracao.TextoValorMaximo = "Valor mínimo é de R$ " + this.configuracao.DoacaoValorMaximo.toString().replace(".", ",");
        console.log(result);
        resolve(true);
      });
    });
  }

  //enviar a doação
  enviarDoacao(model) {

    //this.erroParte4 = true;
    if (!this.validaCampos4(model)) {
      let loading = this.loadingCtrl.create({
        content: 'Aguarde enquanto essa operação é processada…'
      });

      loading.present();

      if (this.model.EspecieRecurso == 4) {

        this.stripe.getCardType(model.NumeroDocumento)
          .then(response => {

            console.log(response);

            if (response == "Visa") {
              this.model.BandeiraDocumento = 1;
            }
            else if (response == "MasterCard") {
              this.model.BandeiraDocumento = 2;
            }

            if (response == "Visa" || response == "MasterCard") {
              // this.model.BandeiraDocumento = 1;
              this.postDoacao(this.model).then((result: any) => {
                // this.erroParte4 = false;
                loading.dismiss();
                if (result.Sucesso == 1) {
                  localStorage.removeItem("doacao");
                  this.goToStep(5, null, false);
                }
                else {

                  this.utils.showModalError('Problema encontrado', result.Mensagem, 'Voltar');
                }
              });
            }
            else {
              // this.erroParte4 = false;
              loading.dismiss();
              this.utils.showModalError('Problema encontrado', "Cartão não permitido ou inválido, verifique seus dados.", 'Voltar');
            }
          });
      }
      else {
        this.postDoacao(this.model).then((result: any) => {
          // this.erroParte4 = false;
          loading.dismiss();
          if (result.Sucesso == 1) {
            localStorage.removeItem("doacao");
            this.goToStep(5, result, false);
          }
          else {
            // this.erroParte4 = false;
            this.utils.showModalError('Problema encontrado', result.Mensagem, 'Voltar');
          }
        });
      }

    }
  }

  postDoacao(model) {
    return new Promise(resolve => {
      this.model.ValidadeDocumento = this.model.MesDocumento + '/' + this.model.AnoDocumento;
      this.model.ClienteId = CONFIG_PROJECT.clienteId;
      this.model.Valor2 = this.model.Valor2.replace('R$ ', '');

      this.doacao.post(model).then((result: any) => {
        resolve(result);

      }, (erro) => {
        console.log(erro);
        resolve({ Mensagem: erro.error.Message });

      });
    });
  }

  //marcar cada passo feito na doação
  putDoacao() {
    return new Promise(resolve => {
      this.doacao.put(this.model).then((result: any) => {
        resolve(result);

      }, (erro) => {
        console.log(erro);
        resolve({ Mensagem: erro.error.Message });
      });
    });
  }

  verificaMensagemSucesso() {

  }

  setEspecieRecurso(recurso) {
    this.model.EspecieRecurso = recurso;
  }

  //lista de anos do cartão de crédito
  loadYears() {
    var y = (new Date()).getFullYear();
    for (var i = 0; i <= 12; i++) {
      this.years.push(y + i);
    }
  }

  //validação (keyup) dos campos do primeiro formulário
  validaPart1() {

    if (this.model.Nome != '' && this.model.Email != '' && this.model.Celular != '' && this.model.CPFCNPJ != '' && this.model.Sexo > 0) {
      this.erroParte1 = 1;
      return true;
    }
    else {
      return false;
    }
  }

  //validação (keyup) dos campos do segundo formulário
  validaPart2() {


    if (this.model.Valor2 != '' &&
      this.model.check1 &&
      this.model.check2) {
      this.erroParte2 = 1;
      return true;
    }
    else {
      return false;
    }
  }

  //validação (keyup) dos campos do terceiro formulário
  validaPart3() {


    if (this.model.CEP != '' &&
      this.model.Endereco != '' &&
      this.model.Cidade != '' &&
      this.model.Estado != '' &&
      this.model.Bairro != '') {
      this.erroParte3 = 1;
      return true;
    }
    else {
      return false;
    }
  }

  validaPart4() {
    //cartão
    if (this.model.EspecieRecurso == 4) {
      if (this.model.NumeroDocumento != '' &&
        this.model.NumeroAutorizacao != '' &&
        this.model.NomeDocumento != '' &&
        (this.model.DataNascimentoDocumento != '' && this.ValidaData(this.model.DataNascimentoDocumento)) &&
        (this.model.MesDocumento != '' && this.model.AnoDocumento != '' && this.validarValidadeCartao())) {
        this.erroParte4 = 1;
        return true;
      }
      else {
        return false;
      }
    }
    //boleto
    else if (this.model.EspecieRecurso == 6) {

    }
  }

  //checa a data de vencimento do cartão
  validarValidadeCartao() {
    var a = this.model.AnoDocumento + "-" + this.model.MesDocumento + "-01";
    var datacartao = new Date(a);
    var datetimenow = new Date();
    if (datetimenow > datacartao) {
      return false;
    }
    else {
      return true;
    }
  }

  //checa se a data de nascimento é válida
  ValidaData(text) {
    var comp = text.split('/');
    var d = parseInt(comp[0], 10);
    var m = parseInt(comp[1], 10);
    var y = parseInt(comp[2], 10);
    var date = new Date(y, m - 1, d);
    if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
      return true;
    } else {
      return false;
    }
  }

  //validação ao clicar dos campos do primeiro formulário
  validaCampos1(model) {

    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let erro = false;


    if (model.Nome == '') {
      this.errorNome = true;
      erro = true;
    } else {
      this.errorNome = false;
    }

    if (model.Celular == '') {
      this.errorCelular = true;
      erro = true;
    } else {
      this.errorCelular = false;
    }
    if (model.Sexo == 0) {
      this.errorSexo = true;
      erro = true;
    } else {
      this.errorSexo = false;
    }

    if (model.Email == '') {
      this.errorEmail = true;
      erro = true;
    }
    else {
      this.errorEmail = false;
    }

    if (model.Email != '' && !re.test(model.Email)) {

      this.errorEmail2 = true;
      erro = true;
    } else {
      this.errorEmail2 = false;
    }

    if (model.CPFCNPJ == '') {
      this.errorCPF = true;
      erro = true;
    } else {
      this.errorCPF = false;
    }

    let validacpf = this.utils.cpf(model.CPFCNPJ.replace(".", "").replace(".", "").replace("-", ""));
    if (model.CPFCNPJ != '' && validacpf == false) {

      this.errorCPF2 = true;
      erro = true;
    }
    else {
      this.errorCPF2 = false;
    }

    if (!erro) {
      this.erroParte1 = 1;
    }
    return erro;
  }

  //validação ao clicar dos campos do segundo formulário
  validaCampos2(model, back) {
    var valor = model.Valor2.replace('R$ ', '');
    let erro = false;

    if (valor == '') {
      this.errorValor = true;
      erro = true;
    } else {
      this.errorValor = false;
    }

    if (parseFloat(this.model.Valor2.replace("R$ ", "")) < this.configuracao.DoacaoValorMinimo) {
      this.errorValor2 = true;
      erro = true;
    } else {
      this.errorValor2 = false;
    }
    if (parseFloat(this.model.Valor2.replace("R$ ", "")) > this.configuracao.DoacaoValorMaximo) {
      this.errorValor3 = true;
      erro = true;
    } else {
      this.errorValor3 = false;
    }

    if (!back) {
      if (!model.check1) {
        this.errorCheck1 = true;
        erro = true;
      } else {
        this.errorCheck1 = false;
      }

      if (!model.check2) {
        this.errorCheck2 = true;
        erro = true;
      } else {
        this.errorCheck2 = false;
      }
    }

    if (!erro) {
      this.erroParte2 = 1;
    }

    return erro;
  }

  //validação ao clicar dos campos do terceiro formulário
  validaCampos3(model, fromstorage) {

    let erro = false;
    if (this.model.CEP == '') {
      if (!fromstorage) {
        this.errorCEP = true;
      }
      erro = true;
    }
    else {
      this.errorCEP = false;
    }
    if (this.model.Endereco == '') {
      if (!fromstorage) {
        this.errorEndereco = true;
      }
      erro = true;
    }
    else {
      this.errorEndereco = false;
    }
    if (this.model.Cidade == '') {
      if (!fromstorage) {
        this.errorCidade = true;
      }
      erro = true;
    }
    else {
      this.errorCidade = false;
    }
    if (this.model.Estado == '') {
      if (!fromstorage) {
        this.errorEstado = true;
      }
      erro = true;
    }
    else {
      this.errorEstado = false;
    }
    if (this.model.Bairro == '') {
      if (!fromstorage) {
        this.errorBairro = true;
      }
      erro = true;
    }
    else {
      this.errorBairro = false;
    }

    if (!erro) {
      this.erroParte3 = 1;
    }

    return erro;
  }

  //validação ao clicar dos campos do quarto formulário
  validaCampos4(model) {
    let erro = false;
    if (this.model.NumeroDocumento == '') {
      this.errorNumeroDocumento = true;
      erro = true;
    }
    else {
      this.errorNumeroDocumento = false;
    }

    if (this.model.NumeroAutorizacao == '') {
      this.errorNumeroAutorizacao = true;
      erro = true;
    }
    else {
      this.errorNumeroAutorizacao = false;
    }

    if (this.model.NomeDocumento == '') {
      this.errorNomeDocumento = true;
      erro = true;
    }
    else {
      this.errorNomeDocumento = false;
    }

    if (this.model.DataNascimentoDocumento == '') {
      this.errorDataNascimentoDocumento = true;
      erro = true;
    }
    else {
      this.errorDataNascimentoDocumento = false;
    }

    if (this.model.DataNascimentoDocumento != '' &&
      !this.ValidaData(this.model.DataNascimentoDocumento)) {
      this.errorDataNascimentoDocumento2 = true;
      erro = true;
    }
    else {
      this.errorDataNascimentoDocumento2 = false;
    }

    if (this.model.DataNascimentoDocumento != '' &&
      this.ValidaData(this.model.DataNascimentoDocumento) &&
      this.calcIdade(this.model.DataNascimentoDocumento) < 18) {
      this.errorDataNascimentoDocumento3 = true;
      erro = true;
    }
    else {
      this.errorDataNascimentoDocumento3 = false;
    }

    if (this.model.MesDocumento == '') {
      this.errorMesDocumento = true;
      erro = true;
    }
    else {
      this.errorMesDocumento = false;
    }

    if (this.model.AnoDocumento == '') {
      this.errorAnoDocumento = true;
      erro = true;
    }
    else {
      this.errorAnoDocumento = false;
    }
    if (!this.validarValidadeCartao()) {
      this.errorDataVencimento = true;
      erro = true;
    }
    else {
      this.errorDataVencimento = false;
    }

    if (!erro) {
      this.erroParte4 = 1;
    }

    return erro;
  }

  calcIdade(data) {
    var d = new Date,
      ano_atual = d.getFullYear(),
      mes_atual = d.getMonth() + 1,
      dia_atual = d.getDate(),
      split = data.split('/'),
      novadata = split[1] + "/" + split[0] + "/" + split[2],
      data_americana = new Date(novadata),
      vAno = data_americana.getFullYear(),
      vMes = data_americana.getMonth() + 1,
      vDia = data_americana.getDate(),
      ano_aniversario = +vAno,
      mes_aniversario = +vMes,
      dia_aniversario = +vDia,
      quantos_anos = ano_atual - ano_aniversario;
    if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
      quantos_anos--;
    }
    return quantos_anos < 0 ? 0 : quantos_anos;
  }

  //pegando dados atuais da doação
  getDoacaoFromStorage() {
    if (localStorage.getItem('doacao') != null) {
      this.model = JSON.parse(localStorage.getItem('doacao'));
      console.log(this.model);

      if (this.model.Valor2.indexOf('R$') == -1) {
        this.model.Valor2 = 'R$ ' + this.model.Valor2;
      }
      //chegando validação dos campos
      this.validaCampos1(this.model);

      this.model.check1 = false;
      this.model.check2 = false;
      //this.validaCampos2(this.model);
      this.validaCampos3(this.model, true);

      this.model.NumeroDocumento = '';
      this.model.NomeDocumento = '';
      this.model.DataNascimentoDocumento = '';
      this.model.MesDocumento = '';
      this.model.AnoDocumento = '';
      this.model.NumeroAutorizacao = '';
    }
  }

  selecionarValor(valor, v) {

    this.valor1 = (v == 1);
    this.valor2 = (v == 2);
    this.valor3 = (v == 3);
    this.valor4 = (v == 4);

    this.model.Valor2 = valor;
  }

  //converção campo valor - segundo formulario
  convert(amount: any) {
    var money = amount.target.value.replace(",", ".");
    this.model.Valor2 = this.currencyPipe.transform(money, 'R$', true, '1.2-2');
    this.validaPart2();
  }

  //buscar cep api
  getCep(cep: string) {
    cep = cep.replace('.', '').replace('-', '');
    if (cep.length == 8) {
      let loading = this.loadingCtrl.create({
        content: 'Carregando...'
      });
      loading.present();
      this.utils.getCep(cep).then((result: any) => {
        loading.dismiss();
        this.model.Endereco = result.logradouro;
        this.model.Cidade = result.localidade;
        this.model.Estado = result.uf;
        this.model.Bairro = result.bairro;
        this.model.Complemento = result.complemento;
        this.validaPart3();
      });
    }
  }

  openTse() {
    let tseModal = this.modalCtrl.create(TseModalPage, {
      showBackdrop: true,
      cssClass: 'modal-box'
    });
    tseModal.present();
  }
}

@Component({
  selector: 'modal-tse',
  templateUrl: 'modal-tse.html'
})
export class TseModalPage {
  constructor(params: NavParams, public navCtrl: NavController, public loadingCtrl: LoadingController) {
  }

  close() {
    this.navCtrl.pop();
  }
}
