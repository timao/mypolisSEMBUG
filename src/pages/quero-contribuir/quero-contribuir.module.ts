import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QueroContribuirPage, TseModalPage } from './quero-contribuir';
import { BrMaskerModule } from 'brmasker-ionic-3';


@NgModule({
  declarations: [
    QueroContribuirPage,
    TseModalPage
  ],
  imports: [
    IonicPageModule.forChild(QueroContribuirPage),
    IonicPageModule.forChild(TseModalPage),
    BrMaskerModule

  ],
})
export class QueroContribuirPageModule {}
