import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticiasNoticiaDetalhePage } from './noticias-noticia-detalhe';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    NoticiasNoticiaDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(NoticiasNoticiaDetalhePage),
    PipesModule
  ],
})
export class NoticiasNoticiaDetalhePageModule {}
