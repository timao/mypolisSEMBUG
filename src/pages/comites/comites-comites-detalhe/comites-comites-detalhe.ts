import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ComiteProvider} from "../../../providers/comite/comite";
import {AuthProvider} from "../../../providers/auth/auth";
import {UtilsProvider} from "../../../providers/utils/utils";
import {SearchbarComponent} from "../../../components/searchbar/searchbar";
import {Comite} from "../../../models/Comite";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";

@IonicPage({
  name: 'comites-detalhe',
  segment: 'comites-detalhe/:id'
})
@Component({
  selector: 'page-comites-comites-detalhe',
  templateUrl: 'comites-comites-detalhe.html',
})
export class ComitesComitesDetalhePage {

  constructor(
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private comiteProvider: ComiteProvider,
    private auth: AuthProvider,
    private sanitizer: DomSanitizer
    ) {
  }

  result: Comite = null;
  hasLatLng: boolean = false;
  googleMapsSrc: SafeUrl = null;

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
  }

  ionViewDidLoad() {
    const comiteId = parseInt(this.navParams.get('id'));
    this.comiteProvider.findById(this.auth.getToken(), comiteId).then(result => {
      this.result = result;
      if(result.Longitude && result.Latitude) {
        this.hasLatLng = true;
        this.googleMapsSrc = this.sanitizer.bypassSecurityTrustResourceUrl('http://maps.google.com/maps?q=' + result.Latitude + ',' + result.Longitude + '&z=15&output=embed');
      }
    },
    err => {
      console.log('error: ', err);
    });
  }
}
