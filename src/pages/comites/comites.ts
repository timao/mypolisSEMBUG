import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController} from 'ionic-angular';
import {SearchbarComponent} from "../../components/searchbar/searchbar";
import {ComiteProvider} from "../../providers/comite/comite";
import { AuthProvider } from '../../providers/auth/auth';
import {Comite} from "../../models/Comite";
import {UtilsProvider} from "../../providers/utils/utils";

@IonicPage({
  name: 'comites',
  segment: 'comites'
})
@Component({
  selector: 'page-comites',
  templateUrl: 'comites.html',
})
export class ComitesPage {

  private itemsPerPage: number = 4;
  private currentPage: number = 1;
  private totalPages: number = 1;
  public items: Comite[] = [];
  private _itemsResult: Comite[] = [];
  public itemsEnded: boolean = false;
  public modelEstado;
  public modelCidade;

  constructor(
    private navCtrl: NavController,
    //private navParams: NavParams,
    private modalCtrl: ModalController,
    private comiteProvider: ComiteProvider,
    private utils: UtilsProvider,
    private auth: AuthProvider) {
  }

  ionViewDidLoad() {
    this.getData();
  }

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present().then().catch();
  }

  getData() {
    this.comiteProvider.findAll(this.auth.getToken()).then((result: Comite[]) => {

      // Filter
      if(this.modelCidade && this.modelEstado) {
        this._itemsResult = this.filterResult(result);
      } else {
        this._itemsResult = result;
      }

      this.totalPages = Math.ceil(this._itemsResult.length / this.itemsPerPage);

      this.items = this._itemsResult.slice(0, this.itemsPerPage);

   

    }, (err) => {
      console.log('Erro:', err);
    });
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }
  doInfinite(infiniteScroll) {
    if(!this.itemsEnded && this.currentPage < this.totalPages) {
      let sliceItems = this._itemsResult.slice(this.currentPage * this.itemsPerPage, ++this.currentPage * this.itemsPerPage);
      this.items = this.items.concat(sliceItems);

      this.currentPage++;
    } else {
      this.itemsEnded = true;
    }

    setTimeout(() => {
      infiniteScroll.complete();
    }, 500);
  }

  openDetail(page:string, id:number) {
    this.navCtrl.push(page, {id: id})
  }

  getNameFromUf(uf: string) {
    return this.utils.getNameFromUf(uf);
  }

  public estados = [];
  public cidades = [];

  changeEstado() {
    if(this.modelEstado) {
      const estado = this.utils.getFromUf(this.modelEstado);
      this.cidades = estado.cidades;
    }
  }

  changeCidade() {
    this.getData();
  }

  filterResult(result: Comite[]) {
    if(result.length) {
      let filtered = result.filter( (elem) => {
        return elem.Cidade == this.modelCidade && elem.Estado == this.modelEstado;
      });
      return filtered;
    }
    return result;
  }

}
