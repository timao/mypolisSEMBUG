import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroPage } from './cadastro';
// import { ModalSucessoPage } from '../modal-sucesso/modal-sucesso';

import { BrMaskerModule } from 'brmasker-ionic-3';


@NgModule({
  declarations: [
    CadastroPage,
    // CadastroModalPage
  ],
  imports: [
    IonicPageModule.forChild(CadastroPage),
    // IonicPageModule.forChild(CadastroModalPage),
    BrMaskerModule
  ],
})
export class CadastroPageModule {}
