import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DadosComplementaresPage } from './dados-complementares';

@NgModule({
  declarations: [
    DadosComplementaresPage,
  ],
  imports: [
    IonicPageModule.forChild(DadosComplementaresPage),
  ],
})
export class DadosComplementaresPageModule {}
