import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParticipacaoEnviarParticipacaoPage } from './participacao-enviar-participacao';

@NgModule({
  declarations: [
    ParticipacaoEnviarParticipacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ParticipacaoEnviarParticipacaoPage),
  ],
})
export class ParticipacaoEnviarParticipacaoPageModule {}
