import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UtilsProvider } from '../../../providers/utils/utils';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { ParticipacaoProvider } from '../../../providers/participacao/participacao';
import { Camera } from '@ionic-native/camera';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ParticipacaoPage } from '../participacao';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ImagePicker } from '@ionic-native/image-picker';


@IonicPage({
  name: 'form-participacao',
  segment: 'form-participacao'
})
@Component({
  selector: 'page-participacao-enviar-participacao',
  templateUrl: 'participacao-enviar-participacao.html',
  providers: [Geolocation, Diagnostic,ImagePicker]
})
export class ParticipacaoEnviarParticipacaoPage {

  tab = "local";
  categorias: any;
  categoriasfilhas: any;
  estados = new Array();
  cidades: any;
  solicitacao = {
    Latitute: 0,
    Longitude: 0,
    Rua: '',
    Numero: '',
    Bairro: '',
    Cidade: '',
    Estado: '',
    SolicitacaoCategoriaId: 0,
    Texto: '',
    Base64Files: []
  };
  erroPart1 = 0.7;
  erroParteFinal = 0.7;
  erroEstado = false;
  erroCidade = false;
  erroRua = false;
  erroNumero = false;
  erroBairro = false;
  ErroTexto = false;

  fileArray = new Array();

  selectOptEstados = {
    title: 'Selecione o estado',
    subTitle: 'Selecione o estado',
    checked: true
  }

  selectOptCidades = {
    title: 'Selecione a cidade',
    subTitle: 'Selecione a cidade',
    checked: true
  }

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public utils: UtilsProvider,
    private geo: Geolocation,
    private participacao: ParticipacaoProvider,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    private diagnostic: Diagnostic,
    private alertCtrl: AlertController,
    private imagePicker: ImagePicker
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipacaoEnviarParticipacaoPage');
    this.getEstados();
    this.getCategorias();
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      //this.choiceCategorie()
      refresher.complete();
    }, 1000);

  }

  saveLocal() {

    if (this.solicitacao.Rua != '' && this.solicitacao.Numero != '' && this.solicitacao.Bairro != '' && this.solicitacao.Cidade != '' && this.solicitacao.Estado != '') {
      this.tab = "categoria";
    }
    else {
      this.validaCampos();
    }
  }

  validaCampos() {
    let erro = false;
    if (this.solicitacao.Rua == '') {
      erro = true;
      this.erroRua = true;
    }
    else {
      this.erroRua = false;
    }
    if (this.solicitacao.Numero == '') {
      this.erroNumero = true;
      erro = true;
    }
    else {
      this.erroNumero = false;
    }
    if (this.solicitacao.Bairro == '') {
      this.erroBairro = true;
      erro = true;
    }
    else {
      this.erroBairro = false;
    }
    if (this.solicitacao.Cidade == '') {
      this.erroCidade = true;
      erro = true;
    }
    else {
      this.erroCidade = false;
    }
    if (this.solicitacao.Estado == '') {
      this.erroEstado = true;
      erro = true;
    }
    else {
      this.erroEstado = false;
    }

    if (!erro) {
      this.erroPart1 = 1;
    }

    return erro;
  }

  validaCampos2() {
    let erro = false;
    if (this.solicitacao.Texto == '') {
      this.ErroTexto = true;
      erro = true;
    }
    else {
      this.ErroTexto = false;
    }

    if (!erro) {
      this.erroParteFinal = 1;
    }

    return erro;
  }

  choiceCategorie(categ) {
    this.categoriasfilhas = categ.CategoriasFilhas;
    if(categ.CategoriasFilhas.length == 0)
    {
      this.solicitacao.SolicitacaoCategoriaId = categ.Id;
      this.tab = "descricao";
    }
    else{
    this.tab = "tipo";
    }

  }

  saveTipe(id) {
    this.solicitacao.SolicitacaoCategoriaId = id;
    this.tab = "descricao";
  }

  getEstados() {
    this.utils.getEstados().then((result: any) => { this.estados = result; }, (error) => { });
  }

  onSelectCidade(uf) {

    this.utils.getCidades(uf).then((result: any) => { this.cidades = result; }, (error) => { });

    if (this.solicitacao.Cidade != '' && this.solicitacao.Cidade != null) {
      this.selectOptCidades.checked = false;
    }
  }

  getLocation() {
    this.diagnostic.isLocationEnabled().then(successCallback => {
      if (successCallback) {
        let loading = this.loadingCtrl.create({
          content: 'Carregando...'
        });
        loading.present();
        var options = { enableHighAccuracy: true };
        this.geo.getCurrentPosition(options).then((position: Geoposition) => {

          console.log(position);

          this.solicitacao.Latitute = position.coords.latitude;
          this.solicitacao.Longitude = position.coords.longitude;

          this.utils.getGeoLocation(this.solicitacao.Latitute, this.solicitacao.Longitude).then((result: any) => {

            for (var i = 0; i < 3; i++) {
              var resultado = result.results[i];
              this.preencheEndereco(resultado);
            }

            loading.dismiss();
          });
        }).catch((error) => {
          console.log('Error getting location', error);
        });
      }
      else {

        this.alertCtrl.create({
          title: '<img src="assets/icon/error.png"/> Erro',
          subTitle: 'GPS desativado',
          buttons: [{ text: 'Ok' }],
          cssClass: 'alertcss'
        }).present();
      }

    }).catch(errorCallback => { this.utils.showModalError('GPS desativado', 'Ative o GPS', 'OK'); });
  }

  preencheEndereco(resultado) {
    resultado.address_components.forEach(address_component => {
      address_component.types.forEach(type => {
        if (type == 'route' && this.solicitacao.Rua == '') {
          this.solicitacao.Rua = address_component.short_name;
        }
        else if (type == 'street_number' && this.solicitacao.Numero == '') {
          this.solicitacao.Numero = address_component.long_name;
        }
        else if (type == 'sublocality_level_1' && this.solicitacao.Bairro == '') {
          this.solicitacao.Bairro = address_component.long_name;
        }
        else if (type == 'administrative_area_level_2' && this.solicitacao.Cidade == '') {
          this.solicitacao.Cidade = address_component.long_name;
        }
        else if (type == 'administrative_area_level_1' && this.solicitacao.Estado == '') {
          this.solicitacao.Estado = address_component.short_name;
        }
      });
    });
  }

  getCategorias() {
    this.participacao.getCategorias().then((result: any) => {
      this.categorias = result;
      console.log(result);
    });
  }

  abrirCamera() {
    this.takePicture(this.camera.PictureSourceType.CAMERA);
  }

  abrirGaleria() {

    let options = {
      maximumImagesCount: 5,
      width: 500,
      height: 500,
      quality: 75,
      outputType: 1 //base64
    }
  
    this.imagePicker.getPictures(options).then(file_uris => {
        for (var i = 0; i < file_uris.length; i++) {
          this.solicitacao.Base64Files.push(file_uris[i]);
      }},
      err => console.log('uh oh')
    );
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 75,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG
    };
    this.camera.getPicture(options).then((imagePath) => {
      this.solicitacao.Base64Files.push(imagePath);
    }, (err) => {
      console.log(err);
      // this.presentToast('Erro ao selecionar a imagem');
    });
  }

  send() {
    if (this.solicitacao.Texto == '') {
      this.ErroTexto = true;
    }
    else {
      let loading = this.loadingCtrl.create({
        content: 'Carregando...'
      });
      loading.present();
      this.participacao.post(this.solicitacao).then((result: any) => {
        loading.dismiss();
        this.utils.showModalSucesso(result.Titulo, result.Mensagem, "OK", data => { this.navCtrl.setRoot(ParticipacaoPage) })
      }, (error) => {
        loading.dismiss();

        this.alertCtrl.create({
          title: '<img src="assets/icon/error.png"/> Erro',
          subTitle: 'Ocorreu um erro, tente mais tarde.',
          buttons: [{ text: 'Ok' }],
          cssClass: 'alertcss'
        }).present();

        console.log(error);
      });
    }
  }
}

