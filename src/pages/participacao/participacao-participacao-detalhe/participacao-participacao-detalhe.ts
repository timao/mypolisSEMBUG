import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ParticipacaoProvider } from '../../../providers/participacao/participacao';
import { UtilsProvider } from '../../../providers/utils/utils';

@IonicPage({
  name: 'participacao-detalhe',
  segment: 'participacao/:id'
})
@Component({
  selector: 'page-participacao-participacao-detalhe',
  templateUrl: 'participacao-participacao-detalhe.html',
})
export class ParticipacaoParticipacaoDetalhePage {
  part: any;
  Texto: string;
  exibirInteracao: boolean = false;
  TextoBotao:string="Interagir";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private participacao: ParticipacaoProvider,
    private loadingCtrl: LoadingController,
   private util : UtilsProvider) {

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.participacao.get(this.navParams.data).then((result: any) => {
      loading.dismiss();
      this.part = result;
      console.log(result);
    }, (error)=> {loading.dismiss();});
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipacaoParticipacaoDetalhePage');
  }

  interagir() {
    if (this.TextoBotao == "Interagir") {

      this.exibirInteracao = true;
      this.TextoBotao = "Enviar"


    }
    else if (this.TextoBotao == "Enviar") {

      this.exibirInteracao = false;

      let loading = this.loadingCtrl.create({
        content: 'Enviando...'
      });
      loading.present();

      this.participacao.put(this.Texto, this.part.Id).then((result: any) => {
        loading.dismiss();
        this.util.showModalSucesso(result.Titulo,result.Mensagem, "OK", null);
        this.TextoBotao = "Interagir";
        this.Texto = '';
        this.participacao.get(result.SolicitacaoId).then((result2: any) => {
          this.part = result2;
        });

      }, (error) => {
        this.Texto = '';
        loading.dismiss();
        this.TextoBotao = "Interagir";
      });
    }
  }
}
