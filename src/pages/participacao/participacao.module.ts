import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParticipacaoPage } from './participacao';
import { ParticipacaoEnviarParticipacaoPageModule } from './participacao-enviar-participacao/participacao-enviar-participacao.module';

@NgModule({
  declarations: [
    ParticipacaoPage,
  ],
  imports: [
    ParticipacaoEnviarParticipacaoPageModule,
    IonicPageModule.forChild(ParticipacaoPage),
  ],
})
export class ParticipacaoPageModule {}
