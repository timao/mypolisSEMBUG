import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoluntarioDetalhePage } from './voluntario-detalhe';

@NgModule({
  declarations: [
    VoluntarioDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(VoluntarioDetalhePage),
  ],
})
export class VoluntarioDetalhePageModule {}
