import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoluntarioPage } from './voluntario';
import { IndiqueAmigosPage } from './indique-amigos';

@NgModule({
  declarations: [
    VoluntarioPage,
    IndiqueAmigosPage
  ],
  imports: [
    IonicPageModule.forChild(VoluntarioPage),
    IonicPageModule.forChild(IndiqueAmigosPage)
  ],
})
export class VoluntarioPageModule {}
