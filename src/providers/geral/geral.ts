import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class GeralProvider {

    constructor(public http: HttpClient, public auth: AuthProvider) {

    }

    findAll() {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Conteudos?TipoConteudoId=14`;
        let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

        return new Promise((resolve, reject) => {
            this.http.get(url, header)
                .map(res => res)
                .subscribe((result: any) => {
                    resolve(result[result.length - 1]);
                },
                (error) => {
                    reject(error);
                });
        });
    }

    // findPrograma() {
    //     var token = this.auth.getToken();
    //     let url = `${CONFIG_PROJECT.baseApi}/Conteudos?TipoConteudoId=16`;
    //     let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    //     return new Promise((resolve, reject) => {
    //         this.http.get(url, header)
    //             .map(res => res)
    //             .subscribe((result: any) => {
    //                 resolve(result);
    //             },
    //             (error) => {
    //                 reject(error);
    //             });
    //     });
    // }
    
    findPrograma() {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Conteudos?TipoConteudoId=16&size=100&paginar=true`;
        let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

        return new Promise((resolve, reject) => {
          this.http.get(url, header)
            .map(res => res)
            .subscribe((result: any) => {
              resolve(result);
            },
            (error) => {
              reject(error);
            });
        });
    }
    
}