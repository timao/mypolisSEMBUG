import { Injectable, Component } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Platform, AlertController } from 'ionic-angular';
import { ConteudoProvider } from '../conteudo/conteudo';
import { UtilsProvider } from '../utils/utils';
import { AppAvailability } from '@ionic-native/app-availability';


@Injectable()
export class SharedService {

    constructor(
        private SocialSharing: SocialSharing,
        public platform: Platform,
        private _alertCtrl: AlertController,
        public conteudoprovider: ConteudoProvider,
        private appAvailability: AppAvailability,
        public utils: UtilsProvider) {
        console.log('Hello SharedServiceProvider Provider');
    }


    SwitchShare(conteudo, rede) {
        // let app
        // if (this.platform.is('ios')) {
        //     if(rede=='facebook'){
        //         app = 'fb://';
        //     }else if(rede=='instagram'){
        //         app = 'instagram://';
        //     }else if(rede=='twitter'){
        //         app = 'twitter://'
        //     }
        // } else if (this.platform.is('android')) {
        //     if(rede=='facebook'){
        //         app = 'fb://';
        //     }else if(rede=='instagram'){
        //         app = 'instagram://';
        //     }else if(rede=='twitter'){
        //         app = 'twitter://'
        //     }
        // }
        
        switch (rede) {
            case 'facebook':
            // this.appAvailability.check(app)
            // .then(
            // (yes) => {
            //     console.log(app + ' is available')
                this.shareViaFacebook(conteudo);
            // },
            // (no) => {
            //     console.log(app + ' is NOT available')
            //     console.log("náo tem instalado");
            //     this._alertCtrl.create({
            //     title: 'Voluntários da Pátria',
            //     subTitle: 'Você não possui o aplicativo para compartilhar',
            //     buttons: ['OK'],
            //     }).present();
            // }
            // )
                break;
            case 'twitter':
            // this.appAvailability.check(app)
            //     .then(
            //     (yes) => {
            //         console.log(app + ' is available')
                    this.shareViaTwitter(conteudo);
            // },
            // (no) => {
            //     console.log(app + ' is NOT available')
            //     console.log("náo tem instalado");
            //     this._alertCtrl.create({
            //     title: 'Voluntários da Pátria',
            //     subTitle: 'Você não possui o aplicativo para compartilhar',
            //     buttons: ['OK'],
            //     }).present();
            // }
            // )
                break;
            case 'instagram':
                this.shareViaInstagram(conteudo);
                break;
            case 'whatsapp':
                this.shareViaWhatsApp(conteudo);
                break;
            default:
                this.sharePicker(conteudo);
                break;
        }
    }

    shareViaEmail(conteudo) {
        this.platform.ready()
            .then(() => {
                this.SocialSharing.canShareViaEmail()
                    .then(() => {
                        this.SocialSharing.shareViaEmail(conteudo.message + ' ' + conteudo.UrlExterna, conteudo.subject, conteudo.sendTo)
                            .then((data) => {
                                console.log('Shared via Email');
                            })
                            .catch((err) => {
                                console.log('Not able to be shared via Email');
                            });
                    })
                    .catch((err) => {
                        console.log('Sharing via Email NOT enabled');
                    });
            });
    }

    shareViaFacebook(conteudo) {
        this.platform.ready()
            .then(() => {
                        this.SocialSharing.shareViaFacebook(conteudo.Titulo, conteudo.Imagem, conteudo.UrlExterna)
                            .then((data) => {
                                console.log('Shared via Facebook');
                                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'facebook')
                                    .then((response: any) => {
                                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                                    }, (error) => {
                                        console.log('não tem facebook')
                                    });

                            })
                            .catch((err) => {
                                console.log('Was not shared via Facebook');
                            });

            });
    }

    shareViaInstagram(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.shareViaInstagram(conteudo.Titulo + ' ' +  conteudo.UrlExterna, conteudo.Imagem)
                    .then((data) => {
                        this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'instagram').then((response: any) => {
                            // setTimeout(() => {
                                this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                            // }, 5000);
                        }, (error) => {
                            console.log('não tem instagram')
                        });
                        console.log('Shared via shareViaInstagram');
                    })
                    .catch((err) => {
                        console.log('Was not shared via Instagram');
                    });

            });
    }

    sharePicker(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.share(conteudo.Titulo, conteudo.UrlExterna, conteudo.Imagem)
                    .then((data) => {
                        console.log('Shared via SharePicker');
                    })
                    .catch((err) => {
                        console.log('Was not shared via SharePicker');
                    });

            });
    }

    shareViaTwitter(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.canShareVia('twitter', conteudo.Titulo + ' ' + conteudo.UrlExterna, conteudo.Imagem)
                    .then((data) => {

                        this.SocialSharing.shareViaTwitter(conteudo.Titulo, conteudo.Imagem)
                            .then((data) => {
                                this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId, 'twitter')
                                    .then((response: any) => {
                                        // setTimeout(() => {
                                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                                        // }, 5000);
                                    }, (error) => {
                                        console.log('não tem twitter')
                                    });
                                console.log('Shared via Twitter');
                            })
                            .catch((err) => {
                                console.log('Was not shared via Twitter');
                            });

                    });

            })
            .catch((err) => {
                console.log('Not able to be shared via Twitter');
            });
    }

    shareViaWhatsApp(conteudo) {
        this.platform.ready()
            .then(() => {
                this.SocialSharing.shareViaWhatsApp(conteudo.Titulo + ' ' + conteudo.UrlExterna, conteudo.Imagem)
                    .then((data) => {
                        this.conteudoprovider.postShare(conteudo.Id, conteudo.AcervoId, conteudo.PessoaId,'whatsapp')
                            .then((response: any) => {
                                // setTimeout(() => {
                                    this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK", null);
                                // }, 5000);
                            }, (error) => {
                                console.log('não tem whats')
                            });
                        console.log('Shared via Twitter');
                    })
                    .catch((err) => {
                        console.log('Was not shared via Twitter');
                    });

            });

    }
}