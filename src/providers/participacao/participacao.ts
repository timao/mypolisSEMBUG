import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class ParticipacaoProvider {

    constructor(public http: HttpClient, public auth: AuthProvider) { }

    getCategorias() {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Participacoes/Categorias`;
        let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };
        return new Promise((resolve, reject) => {
            this.http.get(url, header)
                .map(res => res)
                .subscribe((result: any) => {
                    resolve(result);
                },
                    (error) => {
                        reject(error);
                    });
        });
    }

    post(solicitacao) {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Participacoes/Enviar`;
        let header = { "headers": { "Authorization": `Bearer ${token}` } };

        return new Promise((resolve, reject) => {
            this.http.post(url, solicitacao, header)
                .map(res => res)
                .subscribe((result: any) => {
                    resolve(result);
                },
                    (error) => {
                        reject(error);
                    });
        });
    }

    getAll() {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Participacoes/Minhas`;
        let header = { "headers": { "Authorization": `Bearer ${token}` } };

        return new Promise((resolve, reject) => {
            this.http.get(url, header)
                .map(res => res)
                .subscribe((result: any) => {
                    resolve(result);
                },
                    (error) => {
                        reject(error);
                    });
        });
    }
    get(id: any) {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Participacoes/` + id;
        let header = { "headers": { "Authorization": `Bearer ${token}` } };

        return new Promise((resolve, reject) => {
            this.http.get(url, header)
                .map(res => res)
                .subscribe((result: any) => {
                    resolve(result);
                },
                    (error) => {
                        reject(error);
                    });
        });
    }

    put(texto: string, id: number) {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Participacoes/Interagir`;
        let header = { "headers": { "Authorization": `Bearer ${token}` } };

        let body = {
            Id: id,
            Texto: texto
        };

        return new Promise((resolve, reject) => {
            this.http.post(url, body, header).map(res => res)
                .subscribe((result: any) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                });
        });
    }
}